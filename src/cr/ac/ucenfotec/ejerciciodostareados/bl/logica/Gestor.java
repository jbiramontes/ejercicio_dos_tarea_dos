package cr.ac.ucenfotec.ejerciciodostareados.bl.logica;


import cr.ac.ucenfotec.ejerciciodostareados.bl.entidades.Motor;
import cr.ac.ucenfotec.ejerciciodostareados.bl.entidades.Vehiculo;

import java.util.ArrayList;

/**
 * @author Jorge Biramontes
 * @version 1.0
 */
public class Gestor {

    private ArrayList<Motor> arregloMotores = new ArrayList<>();
    private  ArrayList<Vehiculo> arregloVehiculos= new ArrayList<>();



    public void agregarVehiculo(String serie, String marca, Motor nuevoMotor) {
        Vehiculo nuevoVehiculo = new Vehiculo (serie, marca, nuevoMotor );
        arregloVehiculos.add(nuevoVehiculo);
    }


    public Motor nuevoMotor(String serieMotor, String cilindros) {
        Motor nuevoMotor = new Motor(serieMotor,cilindros);
        return nuevoMotor;

    }


    public ArrayList<Vehiculo> getArregloVehiculos() {
        return arregloVehiculos;
    }


}

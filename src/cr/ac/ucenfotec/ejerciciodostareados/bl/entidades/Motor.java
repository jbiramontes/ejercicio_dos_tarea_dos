package cr.ac.ucenfotec.ejerciciodostareados.bl.entidades;
/**
 * @author Jorge Biramontes
 * @version 1.0
 */
public class Motor {

    private String serieMotor;
    private String numCilindros;

    /**
     *
     * @return Serie del motor
     */
    public String getSerie() {
        return serieMotor;
    }

    /**
     *
     * @param serie Serie del motor
     */
    public void setSerie(String serie) {
        this.serieMotor = serie;
    }

    /**
     *
     * @return numerdo de cilindros del motor
     */
    public String getNumCilindros() {
        return numCilindros;
    }

    /**
     *
     * @param numCilindros numero de cilindros del motor
     */
    public void setNumCilindros(String numCilindros) {
        this.numCilindros = numCilindros;
    }

    /**
     * Constructor sin parametros
     */
    public Motor() {
    }

    /**
     * Constructor con parametros
     * @param serie numero de serie del motor
     * @param numCilindros numCilindros numero de cilindros del motor
     */
    public Motor(String serie, String numCilindros) {
        this.serieMotor = serie;
        this.numCilindros = numCilindros;
    }

    @Override
    public String toString() {
        return ", Motor--" +
                "serie='" + serieMotor + '\'' +
                ", numCilindros='" + numCilindros + '\'' ;
    }
}

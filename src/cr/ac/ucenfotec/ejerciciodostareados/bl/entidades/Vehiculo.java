package cr.ac.ucenfotec.ejerciciodostareados.bl.entidades;
/**
 * @author Jorge Biramontes
 * @version 1.0
 */
public class Vehiculo {

    private String serie;
    private String marca;
    private Motor motor;

    /**
     *
     * @return Serie del vehiculo
     */
    public String getSerie() {
        return serie;
    }

    /**
     *
     * @param serie Serie del vehiculo
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }

    /**
     *
     * @return Marca del vehiculo
     */
    public String getMarca() {
        return marca;
    }

    /**
     *
     * @param marca marca del vehiculo
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     *
     * @return motor del vehiculo
     */
    public Motor getMotor() {
        return motor;
    }

    /**
     *
     * @param motor motor del vehiculo
     */
    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    /**
     * Constrcutor sin parametros
     */
    public Vehiculo() {
    }

    /**
     * Constructor con parametros
     * @param serie Serie del vehiculo
     * @param marca Marca del vehiculo
     * @param motor Motor del vehiculo
     */
    public Vehiculo(String serie, String marca, Motor motor) {
        this.serie = serie;
        this.marca = marca;
        this.motor = motor;
    }

    @Override
    public String toString() {
        return "Vehiculo {" +
                "serie='" + serie + '\'' +
                ", marca='" + marca + '\'' +
                motor +
                '}';
    }
}

package cr.ac.ucenfotec.ejerciciodostareados;

import cr.ac.ucenfotec.ejerciciodostareados.controlador.Controlador;

public class Main {

    public static void main(String[] args) {
        Controlador ejecutar=new Controlador();
        ejecutar.ejecutar();
    }
}

package cr.ac.ucenfotec.ejerciciodostareados.iu;

import java.io.PrintStream;
import java.util.Scanner;

/**
 * @author Jorge Biramontes
 * @version 1.0
 */
public class IU {


    private Scanner input = new Scanner(System.in);
    private PrintStream output = new PrintStream(System.out);

    /**
     * @return opcion numero seleccionado por usuario
     */

    public int leerOpcion() {

        return input.nextInt();

    }

    /**
     * Mensajes para mostrar al usuario
     * @param mensaje mensaje para mostrar a usuario
     */
    public void mensaje(String mensaje) {
        output.println(mensaje);


    }

    /**
     * Lectura de datos ingresados por usuario
     * @return texto ingresado por el usuario
     */
    public String leerDatos() {

        return input.next();

    }
}



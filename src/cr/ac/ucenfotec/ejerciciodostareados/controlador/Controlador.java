package cr.ac.ucenfotec.ejerciciodostareados.controlador;

import cr.ac.ucenfotec.ejerciciodostareados.bl.entidades.Vehiculo;
import cr.ac.ucenfotec.ejerciciodostareados.bl.logica.Gestor;
import cr.ac.ucenfotec.ejerciciodostareados.iu.IU;

import java.util.ArrayList;

/**
 * @author Jorge Biramontes
 * @version 1.0
 */

public class Controlador {


    private IU interfaz = new IU();
    private Gestor gestor = new Gestor();

    /***
     * Ingreso de opcion por parte del usuario
     * @return el entero ingresado por el usuario
     */
    public int opcionSeleccionada() {

        return interfaz.leerOpcion();

    }

    /**
     * Metodo que ejecuta programa
     */
    public void ejecutar() {
        int opcion = 0;

        do {
            interfaz.mensaje("Registre el vehiculo y el motor");
            String marca, serie, serieMotor, numCilindros;
            interfaz.mensaje("Introduzca la marca del vehiculo ");
            marca = interfaz.leerDatos();
            interfaz.mensaje("Introduzca la serie del vehiculo");
            serie = interfaz.leerDatos();
            interfaz.mensaje("Introduzca la cantidad de cilindros del motor");
            numCilindros = interfaz.leerDatos();
            interfaz.mensaje("Introduzca la serie del MOTOR");
            serieMotor = interfaz.leerDatos();
            //gestor.repetidos(otroEmpleado, gestor.arregloEmpleados, nombre);
            gestor.agregarVehiculo(serie, marca, gestor.nuevoMotor(serieMotor, numCilindros));
            interfaz.mensaje("Registro exitoso");
            interfaz.mensaje("Registre el vehiculo si no desea registrar mas digite 0");
            opcion = opcionSeleccionada();

        } while (opcion != 0);
        interfaz.mensaje("Lista de vehiculos registrados con detalles de su motor");
        ListarComputadoras();
        interfaz.mensaje("Gracias por utilizar nuestro servicio");
    }

    /**
     * Extrae los datos de los vehiculos registrados y su motor
     */
    private void ListarComputadoras() {
        ArrayList<Vehiculo> lista = gestor.getArregloVehiculos();
        for (Vehiculo e : lista) {
            interfaz.mensaje(e.toString());

        }
    }


}



